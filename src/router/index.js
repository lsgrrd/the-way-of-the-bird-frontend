import { createWebHistory, createRouter } from "vue-router"
import Login from "../views/Login.vue"
import Home from "../views/Home.vue"
import Countries from "../views/Countries.vue"
import Country from "../views/Country.vue"

const routes = [
  {
    path: "/",
    name: "Login",
    component: Login,
  }, {
    path: "/home",
    name: "Home",
    component: Home,
  }, {
    path: "/countries",
    name: "Countries",
    component: Countries,
  },{
    path: "/country/:name",
    name: "Country",
    component: Country,
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior(to, from, savedPosition) {
    // always scroll to top
    return { top: 0 }
  },
});

export default router;